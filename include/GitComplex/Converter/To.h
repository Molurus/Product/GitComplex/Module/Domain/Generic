/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Utility/ForwardAs.h>

/*!
 * \file include/GitComplex/Converter/To.h
 * \brief include/GitComplex/Converter/To.h
 */

namespace GitComplex::Converter
{

using Utility::ForwardAs;

template <class T, class U>
using RequireSame = std::enable_if_t<std::is_same_v<std::decay_t<T>, U>, int>;

/*!
 * \struct To
 * \brief To.
 *
 * \headerfile Converter/To.h
 *
 * To.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[Framework::Generic]]
 */
template <class T>
struct To {};

/*!
 * to.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[Framework::Generic]]
 */
template <class To, class From>
inline
To to(From&& from)
{ return convert(std::forward<From>(from), Converter::To<To>{}); }

} // namespace GitComplex::Converter
