/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <otn/all.hpp>

/*!
 * \file include/GitComplex/Utility/FunctorBuilder.h
 * \brief include/GitComplex/Utility/FunctorBuilder.h
 */

namespace GitComplex::Utility
{

/*!
 * The analog of the call: object.method(args...);.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[Framework::Generic]]
 */
template <class Object, class Method, class ... Args>
inline
auto makeFunctor(Object&& object, Method method, Args ... args)
{
    return [object = std::forward<Object>(object),
            method = method,
            args = std::make_tuple(std::forward<Args>(args) ...)]() mutable
           {
               // TODO: Fix a return on the 'else' branch.
               return otn::access(object, [&](auto& object)
            {
                return std::apply(method,
                                  std::tuple_cat(std::forward_as_tuple(object),
                                                 std::move(args)));
            });
           };
}

/*!
 * The analog of the call: action(object.method(args...));.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[Framework::Generic]]
 */
template <class Action, class Object, class Method, class ... Args>
inline
auto makeActionFunctor(Action action, Object&& object, Method method, Args ... args)
{
    return [action = std::forward<Action>(action),
            object = std::forward<Object>(object),
            method = method,
            args = std::make_tuple(std::forward<Args>(args) ...)]() mutable
           {
               // TODO: Fix a return on the 'else' branch.
               return otn::access(object, [&](auto& object)
            {
                return action(std::apply(method,
                                         std::tuple_cat(std::forward_as_tuple(object),
                                                        std::move(args))));
            });
           };
}

} // namespace GitComplex::Utility
