/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <functional>

/*!
 * \file include/GitComplex/Utility/Id.h
 * \brief include/GitComplex/Utility/Id.h
 */

namespace GitComplex::Utility
{

/*!
 * \class Id
 * \brief Id.
 *
 * \headerfile GitComplex/Utility/Id.h
 *
 * Id.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[Framework::Generic]]
 */
template <class T>
class Id
{
public:
    using Value = T;

    const Value& value() const { return m_value; }
    operator const Value&() const { return value(); }

    /*!
     * \class Factory
     * \brief Factory.
     *
     * \headerfile GitComplex/Utility/Id.h
     *
     * Factory.
     *
     * \note [[Abstraction::Detail]] [[Viper::Entity]] [[Framework::Generic]]
     */
    class Factory
    {
    public:
        using Id = Utility::Id<Value>;

    protected:
        static Id makeId(Value value) { return Id{std::move(value)}; }
    };

private:
    explicit Id(Value value) : m_value{std::move(value)} {}

    Value m_value;
};

} // namespace GitComplex::Utility

namespace std
{

template <class T>
struct hash<::GitComplex::Utility::Id<T>>
    : public hash<typename ::GitComplex::Utility::Id<T>::Value>
{};

} // namespace std
