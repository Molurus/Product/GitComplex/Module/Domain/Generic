/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <otn/all.hpp>

/*!
 * \file include/GitComplex/Utility/ActionBuilder.h
 * \brief include/GitComplex/Utility/ActionBuilder.h
 */

namespace GitComplex::Utility
{

/*!
 * makeDirectAction.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[Framework::Generic]]
 */
template <class Object, class ReturnValue, class ... Args>
inline
auto makeDirectAction(Object* object, ReturnValue (Object::* method)(Args ...))
{
    return [ = ](Args ... args)
           { std::invoke(method, object, std::forward<Args>(args) ...); };
}

/*!
 * makeDirectAction.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[Framework::Generic]]
 */
template <class Object, class ReturnValue, class ... Args>
inline
auto makeDirectAction(Object& object, ReturnValue (Object::* method)(Args ...))
{ return makeDirectAction(std::addressof(object), method); }

/*!
 * makeDirectAction.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[Framework::Generic]]
 */
template <class Object, class ReturnValue, class ... Args>
inline
auto makeDirectAction(otn::weak_optional<Object> object,
                      ReturnValue (Object::*     method)(Args ...))
{
    return [ =, object = std::move(object)](Args ... args)
           {
               // TODO: Fix a return on the 'else' branch.
               otn::access(object, [&](auto& object)
                           { std::invoke(method, object, std::forward<Args>(args) ...); });
           };
}

} // namespace GitComplex::Utility
