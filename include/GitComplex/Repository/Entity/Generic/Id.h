/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Core/Id.h>

#include <optional>

/*!
 * \file include/GitComplex/Repository/Entity/Generic/Id.h
 * \brief include/GitComplex/Repository/Entity/Generic/Id.h
 */

namespace GitComplex::Repository::Entity::Generic
{

/*!
 * The identifier of a repository.
 *
 * \note [[Abstraction::DataType]] [[Viper::Entity]] [[Framework::Generic]]
 */
using Id = Core::Id<std::size_t>;

/*!
 * Single \ref Generic::Id "Id".
 *
 * \note [[Abstraction::DataType]] [[Viper::Entity]] [[Framework::Generic]]
 */
using SingleId = Id;

/*!
 * Optional \ref Generic::Id "Id".
 *
 * \note [[Abstraction::DataType]] [[Viper::Entity]] [[Framework::Generic]]
 */
using OptionalId = std::optional<Id>;

/*!
 * List of \ref Generic::Id "Id".
 *
 * \note [[Abstraction::DataType]] [[Viper::Entity]] [[Framework::Generic]]
 */
using IdList = std::vector<SingleId>;

} // namespace GitComplex::Repository::Entity::Generic
